﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crearCarro : MonoBehaviour {
       
    public float velocidad;
    int detener = 1;

    int rnd;
    //Posicion inicial carro
    int[] cordX = new int[] { 100, 21};
    int[] cordY = new int[] { -8, 31};
 
    int x;
    int y;
    int dirx;
    int diry;
 
	// Use this for initialization
	void Start () 
    {     
        velocidad = (Random.Range(10f, 15f));
        rnd = Random.Range(0, 2);
        x = cordX[rnd];
        y = cordY[rnd];
        this.transform.position = new Vector3(x, y, 0);
       

        print("Random "+rnd);
        if (rnd == 0) {

            dirx = -1;
            diry = 0;
           
        }
        if (rnd == 1)
        {
            dirx = 0;
            diry = -1;

            transform.Rotate(new Vector3(0, 0, 90), Space.World);
        }

	}

	// Update is called once per frame
    void Update()
    {
        this.transform.Translate(-velocidad * Time.deltaTime * detener, 0, 0);
    }

    public void Pause()
    {
        detener = 0;
    }

    public void Resume()
    {
        detener = 1;
    }
}
