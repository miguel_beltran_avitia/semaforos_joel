﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calle2 : MonoBehaviour {
    public GameObject calle2;
    public int aux;

    void OnTriggerEnter2D(Collider2D collider)
    {
        calle2.GetComponent<Variables>().Calle2 += aux;
    }
}
