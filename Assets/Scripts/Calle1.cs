﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calle1 : MonoBehaviour {
    public GameObject calle1;
    public int aux;

    void OnTriggerEnter2D(Collider2D collider) {
        calle1.GetComponent<Variables>().Calle1 += aux;
    }
}
