﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambiarSemaforo : MonoBehaviour {
    //public bool verdeSem1;
    //public bool verdeSem2;
    public GameObject semaforo2;
    
    int a = 0;
    int b = 1;
    int c;
    int d = 1;

    Variables Variables;
    int Calle1;
    int Calle2;
 
    
    //Controlar cambio de semaforo
    SpriteRenderer sr;
    SpriteRenderer sr2; 
    public Sprite verde;
    public Sprite amarillo;
    public Sprite rojo;

    //Reanudar carro uno
    public GameObject Checar;
    public GameObject Checar2;
    List<GameObject> reanudarCarros1;
    List<GameObject> reanudarCarros2;

    void Start()
    {   
        Variables = GameObject.Find("interseccion").GetComponent<Variables>();
        sr = GetComponent<SpriteRenderer>();
        sr2 = semaforo2.GetComponent<SpriteRenderer>();
        reanudarCarros1 = Checar.GetComponent<Checar>().reanudarCarros1;
        reanudarCarros2 = Checar2.GetComponent<Checar2>().reanudarCarros2; 
    }

	// Update is called once per frame
	void Update() {
    
        Calle1 = Variables.getCalle1();
        Calle2 = Variables.getCalle2();

        if (Calle1 >= Calle2 )
        {
            if (b == 1&& d==1) 
            {
                StartCoroutine(Sem2());
            }
        }
        else 
        {
            if (a == 1 && c == 1)
            {
                StartCoroutine(Sem1());
            }
        } 
     
	}

    private IEnumerator Sem1()
    {
        c = 0;
        sr.sprite = amarillo;
        yield return new WaitForSeconds(2.0f);
        sr.sprite = rojo;
        sr2.sprite = verde;

        for (var i = 0; i < reanudarCarros2.Count; i++)
        {
            reanudarCarros2[i].GetComponent<crearCarro>().Resume();

            yield return new WaitForSeconds(1.0f);
        }

        reanudarCarros2.Clear();

        b = 1;
        a = 0;
        d = 1;
    }

    private IEnumerator Sem2()
    {
        d = 0;
        sr2.sprite = amarillo;
        yield return new WaitForSeconds(2.0f);
        sr2.sprite = rojo;
        sr.sprite = verde;
        print("capacidad " + reanudarCarros1.Capacity);
        print("count " + reanudarCarros1.Count);
            for (var i = 0; i < reanudarCarros1.Count; i++)
            {
                reanudarCarros1[i].GetComponent<crearCarro>().Resume();

                yield return new WaitForSeconds(1.0f);
            }

            reanudarCarros1.Clear();
        
        b = 0;
        a = 1;
        c = 1;
    }


}
