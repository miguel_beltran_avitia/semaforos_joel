﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Variables : MonoBehaviour {
    public int Calle1;
    public int Calle2;

    float porcientoCalle1;
    float porcientoCalle2;
    float Total;

    void Update()
    {
        Total = Calle1 + Calle2;
        porcientoCalle1 = Calle1 / Total;
        porcientoCalle2 = Calle2 / Total;

    }

    public int getCalle1()
    {
        return Calle1;
    }

    public int getCalle2()
    {
        return Calle2;
    }

    public float getPorcientoCalle1()
    {
        return porcientoCalle1;
    }

    public float getPorcientoCalle2()
    {
        return porcientoCalle2;
    }
}
