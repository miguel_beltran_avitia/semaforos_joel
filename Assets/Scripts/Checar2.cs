﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checar2 : MonoBehaviour {
    public GameObject semaforo2;
    SpriteRenderer sr2;
    public Sprite verde;

    //lista de carros para reanudar
    public List<GameObject> reanudarCarros2 = new List<GameObject>();
    void Start()
    {

        sr2 = semaforo2.GetComponent<SpriteRenderer>();

    }
    void OnTriggerEnter2D(Collider2D collider)
    {
      
        GameObject carro = collider.gameObject;

        if (sr2.sprite == verde)
        {
            print("Resumen2");
           // carro.GetComponent<crearCarro>().Resume();

        }
        else
        {
            print("Pause2");
            carro.GetComponent<crearCarro>().Pause();
            reanudarCarros2.Add(carro);
        }


    }
}
