﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {
    public GameObject semaforo2;
    void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject carro = collider.gameObject;
        Destroy(carro);
    }
}
